'use strict';

module.exports = function(app) {
  var article = require('../controllers/articleController');
  var comment = require('../controllers/commentController');

  // article routes
  app.route('/articles/:limit/:page')
    .get(article.all);
  app.route('/articles')
    .post(article.create);

  app.route('/articles/:id')
    .get(article.show);

  // comment routes
  app.route('/comments')
    .post(comment.create);

  app.route('/comments/:articleID')
    .get(comment.allByArticle);
}
