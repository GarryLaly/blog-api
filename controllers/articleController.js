'use strict';

var mongoose = require('mongoose');
var Article = mongoose.model('Articles');

exports.all = function(req, res) {
  Article.paginate({}, { page: parseInt(req.params.page), limit: parseInt(req.params.limit) }, function(err, article) {
    if (err) {
      res.send(err);
    }
    res.json(article);
  });
};

exports.create = function(req, res) {
  var newData = new Article(req.body);
  newData.save(function(err, article) {
    if (err) {
      res.send(err);
    }
    res.json(article);
  });
};

exports.show = function(req, res) {
  Article.findById(req.params.id, function(err, article) {
    if (err) {
      res.send(err);
    }
    res.json(article);
  });
};
