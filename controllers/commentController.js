'use strict';

var mongoose = require('mongoose');
var Comment = mongoose.model('Comments');

exports.allByArticle = function(req, res) {
  Comment.find({article_id: req.params.articleID}, function(err, comment) {
    if (err) {
      res.send(err);
    }
    res.json(comment);
  });
};

exports.create = function(req, res) {
  var newData = new Comment(req.body);
  newData.save(function(err, comment) {
    if (err) {
      res.send(err);
    }
    res.json(comment);
  });
};
