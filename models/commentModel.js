'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
  article_id: {
    type: Schema.Types.ObjectId,
    ref: 'Articles',
  },
  reply_id: {
    type: Schema.Types.ObjectId,
    ref: 'Comments',
  },
  username: {
    type: String,
    required: 'Username cannot empty',
  },
  content: {
    type: String,
    required: 'Content cannot empty',
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('Comments', CommentSchema);
