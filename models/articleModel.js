'use strict';
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var ArticleSchema = new Schema({
  author: {
    type: String,
    required: 'Author cannot empty',
  },
  title: {
    type: String,
    required: 'Title cannot empty',
  },
  content: {
    type: String,
    required: 'Content cannot empty',
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
});

ArticleSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Articles', ArticleSchema);
