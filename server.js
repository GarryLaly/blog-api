'use strict';

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

// Call models
const Article = require('./models/articleModel');
const Comment = require('./models/commentModel');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();

// MongoDB connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://mongo/blogs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Register the routing
var routes = require('./routes/route');
routes(app);

app.get('/', (req, res) => {
  res.send('Blog API by Garry Priambudi');
});

app.listen(PORT, HOST);
console.log(`Running Blog API on http://${HOST}:${PORT}`);
